The repository with the potential files related to the article [Towards atomistic modelling of solid Pb-O formation and dissolution in liquid lead coolant: Interatomic potential development](https://www.sciencedirect.com/science/article/pii/S0022311524001193). DOI `10.1016/j.jnucmat.2024.155016`.

If you use any files from the repository for academic purposes, you must cite the [article](https://www.sciencedirect.com/science/article/pii/S0022311524001193). Commercial use is prohibited. 
