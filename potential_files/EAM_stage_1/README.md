This folder consists files with the EAM potential for Pb (first stage):

`Lead.EAM.potfit` - potfit potential file
`Lead.EAM.lammps` - file for LAMMPS to use with `eam/alloy` style
