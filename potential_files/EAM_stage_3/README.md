This folder consists with the EAM potential for Pb-O (third stage) that describes the oxygen solubility limit in molten lead:

`OPb.eam.alloy` - file for LAMMPS to use with `eam/alloy` style
