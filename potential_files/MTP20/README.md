This folder contains file with the MTP20 potential for Pb-O system. This potential could be used with LAMMPS software. As the standart version of LAMMPS does not support MTP, the [interface](https://gitlab.com/ashapeev/interface-lammps-mlip-2/) has to be installed. 

In LAMMPS script the `pair/style` could be described as follows:

```
pair_style      mlip mlip.ini
pair_coeff      * *
mass            1 16.0
mass            2 207.2
```

Oxygen will be type 1 and Lead - type 2.


