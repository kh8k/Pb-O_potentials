This folder consists files with the EAM potential for Pb-O (second stage):

`Lead_O.EAM.potfit` - potfit potential file
`Lead_O.EAM.lammps` - file for LAMMPS to use with `eam/alloy` style
